#!/bin/bash
#

git submodule update --init --recursive

#  ______________________
# | THEME BASE STRUCTURE |
# '----------------------'

TARGET=./build/Win1X
TARGET_DARK=./build/Win1X-dark
mkdir -p "$TARGET"
mkdir -p "$TARGET_DARK"
cp index.theme $TARGET/index.theme
cp index-dark.theme $TARGET_DARK/index.theme

# base on Win11 sans actions, panel and status icons
echo "Prepare base theme from Win11 ..."
cp -r src/Win11-icon-theme/src/{animations,apps,categories,devices,emblems,mimes,places} $TARGET/ 2>/dev/null
cp -rPu src/Win11-icon-theme/links/{apps,categories,devices,emblems,mimes,places} $TARGET/ 2>/dev/null
pushd $TARGET > /dev/null
ln -sfn actions actions@2x
ln -sfn animations animations@2x
ln -sfn apps apps@2x
ln -sfn categories categories@2x
ln -sfn devices devices@2x
ln -sfn emblems emblems@2x
ln -sfn mimes mimes@2x
ln -sfn panel panel@2x
ln -sfn places places@2x
ln -sfn status status@2x
popd > /dev/null

echo "Prepare dark theme structure ..."
pushd $TARGET_DARK > /dev/null
mkdir -p actions categories/symbolic panel places status \
devices/16 devices/22 devices/24 devices/symbolic
ln -sfn actions actions@2x
ln -sfn categories categories@2x
ln -sfn devices devices@2x
ln -sfn panel panel@2x
ln -sfn places places@2x
ln -sfn status status@2x
popd > /dev/null

#  _____________________________
# | SYMBOLIC ICONS REPLACEMENTS |
# '-----------------------------'

# take actions, panel and status icons from Tela
echo "Importing symbolic icons from Tela ..."
rm -rf $TARGET/places/{16,22,24,symbolic}
mkdir -p $TARGET/{actions,panel,status}/{16,22,24,32}
# incorporate 'places', where we only want to keep the size 48 directory and
# replace all symbolic ones
for CAT in actions status panel places
do
    for SIZE in 16 22 24 32 symbolic
    do
        for DEST in "$TARGET" "$TARGET_DARK"
        do
            # -P = never follow symlinks, directly copy them
            # we use --remove-destination to replace symlinks by actual files
            mkdir -p $DEST/$CAT/$SIZE/
            cp -P --remove-destination src/Tela-icon-theme/src/$SIZE/$CAT/* $DEST/$CAT/$SIZE/ 2>/dev/null
            cp -P src/Tela-icon-theme/links/$SIZE/$CAT/* $DEST/$CAT/$SIZE/ 2>/dev/null
            # try discarding empty dirs
            rmdir $DEST/$CAT/$SIZE 2>/dev/null
        done
    done
done
# re-insert specific icons
cp src/Win11-icon-theme/src/places/16/folder.svg $TARGET/places/16/
cp src/Win11-icon-theme/src/places/16/folder.svg $TARGET_DARK/places/16/
# Tela symbolic icons for existing categories
for CAT in mimes devices
do
    # clear any remnants of the base theme
    rm -f $TARGET/$CAT/$SIZE/*
    rm -f $TARGET_DARK/$CAT/$SIZE/*
    # we need a workaround here because 'mimes' is called 'mimetypes' in Tela
    TELA_CAT=$CAT
    if [ "$CAT" == "mimes" ]; then
        TELA_CAT="mimetypes"
    fi
    for SIZE in 16 22 24 symbolic
    do
        for DEST in "$TARGET" "$TARGET_DARK"
        do
            # -P = never follow symlinks, directly copy them
            # we use --remove-destination to replace symlinks by actual files
            cp -P --remove-destination src/Tela-icon-theme/src/$SIZE/$TELA_CAT/* $DEST/$CAT/$SIZE/ 2>/dev/null
            cp -P src/Tela-icon-theme/links/$SIZE/$TELA_CAT/* $DEST/$CAT/$SIZE/ 2>/dev/null
        done
    done
done
# Tela symbolic icons for 'categories' and 'apps'
rm -f $TARGET/categories/symbolic/*
cp -P src/Tela-icon-theme/src/symbolic/categories/* $TARGET/categories/symbolic/
cp -P src/Tela-icon-theme/links/symbolic/categories/* $TARGET/categories/symbolic/
rm -f $TARGET/apps/symbolic/*
cp -P src/Tela-icon-theme/src/symbolic/apps/* $TARGET/apps/symbolic/
cp -P src/Tela-icon-theme/links/symbolic/apps/* $TARGET/apps/symbolic/
# DARK THEME
rm -f $TARGET_DARK/categories/symbolic/*
cp -P src/Tela-icon-theme/src/symbolic/categories/* $TARGET_DARK/categories/symbolic/
cp -P src/Tela-icon-theme/links/symbolic/categories/* $TARGET_DARK/categories/symbolic/

#  ___________________________
# | SYMBOLIC ICONS RECOLORING |
# '---------------------------'

echo "Recoloring Tela panel icons ..."
sed -i "s/dfdfdf/565656/gI" $TARGET/panel/{16,22,24}/*.svg

echo "Recoloring Tela icons for dark theme ..."
# clear any orphaned symlinks beforehand
find $TARGET_DARK/{actions,categories,devices,panel,places,status} -xtype l -exec rm {} \;
sed -i "s/dfdfdf/CFCFCF/gI" $TARGET_DARK/panel/{16,22,24}/*.svg
sed -i -E "s/(555555|565656|727272)/CFCFCF/gI" $TARGET_DARK/actions/{16,22,24,symbolic}/*.svg
sed -i -E "s/(555555|565656|727272)/CFCFCF/gI" $TARGET_DARK/categories/symbolic/*.svg
sed -i -E "s/(555555|565656|727272)/CFCFCF/gI" $TARGET_DARK/devices/{16,22,24,symbolic}/*.svg
sed -i -E "s/(555555|565656|727272)/CFCFCF/gI" $TARGET_DARK/places/{16,22,24,symbolic}/*.svg
sed -i -E "s/(555555|565656|727272)/CFCFCF/gI" $TARGET_DARK/status/symbolic/*.svg

# Link unmodified sub-dirs from base to dark theme
# this retains the base theme's colored icons
pushd $TARGET_DARK > /dev/null
ln -sfn ../../Win1X/places/48 places/48
ln -sfn ../../Win1X/devices/scalable devices/scalable
ln -sfn ../../Win1X/categories/32 categories/32

# instead of simply omitting, we also link completely unmodified main dirs
# this prevents symbolic icons in other categories taking sudden precedence
# over icons present in the inherited theme but in another category
# e.g. the symbolic actions/*/multimedia-volume-control.svg will override
# any apps/scalable/multimedia-volume-control.svg if the latter is only present
# in the inherited theme
ln -sfn ../Win1X/animations animations
ln -sfn ../Win1X/apps apps
ln -sfn ../Win1X/emblems emblems
ln -sfn ../Win1X/mimes mimes
popd > /dev/null

#  __________________________
# | OTHER THEME ICON IMPORTS |
# '--------------------------'

# app icons (and symlinks pointing to them) to import from Tela instead
declare -a ICONS_FROM_TELA=(
accessories-character-map
kid3
virtualbox
)
# app icons (and symlinks pointing to them) to import from We10X instead
declare -a ICONS_FROM_WE10X=(
workspace-switcher
workspace-switcher-top-left
workspace-switcher-right-top
workspace-switcher-left-bottom
workspace-switcher-right-bottom
preferences-desktop-display
accessories-document-viewer
computerjanitor
wine
wine-uninstaller
wine-winecfg
wine-winetricks
virt-manager
konqueror
flash
gimp
smplayer
simplescreenrecorder
compton
blender
system-search
)
# app icons (and symlinks pointing to them) to import from Fluent instead
declare -a ICONS_FROM_FLUENT=(
kdenlive
panel
filezilla
redshift
meld
qt
qtcreator
qtdesigner
)
# app icons (and symlinks pointing to them) to import from Papirus instead
declare -a ICONS_FROM_PAPIRUS=(
accessories-ebook-reader
calibre-ebook-edit
calibre-viewer
)
echo "Importing selected icons from Tela ..."
for ICON in ${ICONS_FROM_WE10X[@]}
do
    FILE=$ICON.svg
    echo " - importing $ICON ..."
    rm -f $TARGET/apps/scalable/$FILE
    cp -f src/We10X-icon-theme/src/apps/scalable/$FILE $TARGET/apps/scalable/
    python3 ./pull_any_symlinks.py "src/We10X-icon-theme/links/apps/scalable" "$FILE" "$TARGET/apps/scalable/"
done
echo "Importing selected icons from We10X ..."
for ICON in ${ICONS_FROM_TELA[@]}
do
    FILE=$ICON.svg
    echo " - importing $ICON ..."
    rm -f $TARGET/apps/scalable/$FILE
    cp -f src/Tela-icon-theme/src/scalable/apps/$FILE $TARGET/apps/scalable/
    python3 ./pull_any_symlinks.py "src/Tela-icon-theme/links/scalable/apps" "$FILE" "$TARGET/apps/scalable/"
done
echo "Importing selected icons from Fluent ..."
for ICON in ${ICONS_FROM_FLUENT[@]}
do
    FILE=$ICON.svg
    echo " - importing $ICON ..."
    rm -f $TARGET/apps/scalable/$FILE
    cp -f src/Fluent-icon-theme/src/scalable/apps/$FILE $TARGET/apps/scalable/
    python3 ./pull_any_symlinks.py "src/Fluent-icon-theme/links/scalable/apps" "$FILE" "$TARGET/apps/scalable/"
done
echo "Importing selected icons from Papirus ..."
for ICON in ${ICONS_FROM_PAPIRUS[@]}
do
    FILE=$ICON.svg
    echo " - importing $ICON ..."
    rm -f $TARGET/apps/scalable/$FILE
    cp -f src/papirus-icon-theme/Papirus/64x64/apps/$FILE $TARGET/apps/scalable/
    python3 ./pull_any_symlinks.py "src/papirus-icon-theme/Papirus/64x64/apps" "$FILE" "$TARGET/apps/scalable/"
done

#  _______________________
# | CUSTOM TWEAKS SECTION |
# '-----------------------'

echo "Apply custom tweaks ..."
# replace Tela's kid3 icon by Tela's easytag icon
cp -f --remove-destination src/Tela-icon-theme/src/scalable/apps/easytag.svg $TARGET/apps/scalable/kid3.svg
# Xpad sticky notes app and tray icon
cp -f --remove-destination src/We10X-icon-theme/src/apps/scalable/accessories-text-editor.svg $TARGET/apps/scalable/xpad.svg
# GTK/Gnome updater icons
cp -f src/We10X-icon-theme/src/apps/scalable/yast-upgrade.svg $TARGET/apps/scalable/update-manager.svg
cp -f src/We10X-icon-theme/src/apps/scalable/uget-icon.svg $TARGET/apps/scalable/software-properties.svg
# We10X calculator icon
cp -f src/We10X-icon-theme/src/apps/scalable/calc.svg $TARGET/apps/scalable/calc.svg
# restore some older icon revisions that fit better
pushd src/Win11-icon-theme > /dev/null
git show ca66a81:src/apps/scalable/keyring-manager.svg > ../../$TARGET/apps/scalable/keyring-manager.svg
git show 57165ae:src/apps/scalable/utilities-log-viewer.svg > ../../$TARGET/apps/scalable/utilities-log-viewer.svg
popd > /dev/null
# insert start button icon
cp -f ./start-here.svg $TARGET/places/48/
# add some custom symlinks
pushd $TARGET/apps/scalable > /dev/null
ln -sf preferences-desktop-emoticons.svg sleek.svg
ln -sf system-search.svg preferences-system-search.svg
ln -sf computerjanitor.svg bleachbit.svg
ln -sf search.svg edit-find.svg
ln -sf search.svg xfce4-appfinder.svg
ln -sf search.svg org.xfce.appfinder.svg
ln -sf search.svg catfish.svg
ln -sf file-manager.svg thunar.svg
ln -sf alacarte.svg menulibre.svg
ln -sf keyring-manager.svg keepassxc.svg
ln -sf applications-games.svg lutris.svg
popd > /dev/null

# unlink status/symbolic/dialog-warning-symbolic.svg
# unlink status/symbolic/dialog-information-symbolic.svg
# unlink status/symbolic/dialog-error-symbolic.svg
# unlink status/symbolic/dialog-question-symbolic.svg
# cp -f status/32/dialog-warning.svg status/symbolic/dialog-warning-symbolic.svg
# cp -f status/32/dialog-information.svg status/symbolic/dialog-information-symbolic.svg
# cp -f status/32/dialog-error.svg status/symbolic/dialog-error-symbolic.svg
# cp -f status/32/dialog-question.svg status/symbolic/dialog-question-symbolic.svg

# inkscape -z -w 48 -h 48 status/32/dialog-warning.svg -e status/32/dialog-warning-symbolic.png
# inkscape -z -w 48 -h 48 status/32/dialog-information.svg -e status/32/dialog-information-symbolic.png
# inkscape -z -w 48 -h 48 status/32/dialog-error.svg -e status/32/dialog-error-symbolic.png
# inkscape -z -w 48 -h 48 status/32/dialog-question.svg -e status/32/dialog-question-symbolic.png


echo "Cleaning up any broken/orphaned symlinks ..."
find $TARGET/{actions,animations,apps,categories,devices,emblems,mimes,panel,places,status} -xtype l -exec rm {} \;

echo "Updating icon cache ..."
gtk-update-icon-cache $TARGET
gtk-update-icon-cache $TARGET_DARK
echo "DONE"