# Win1X icon theme

This icon theme is a custom combination of the following popular icon themes that imitate the look of a popular proprietary OS.
The repository contains the build and install scripts as well as a snapshot of the most recent build.
All sources where icons are taken from during the build process are included as submodules:

- base theme for device and application icons:
    - [Win11 icon theme](https://github.com/yeyushengfan258/Win11-icon-theme) by yeyushengfan258
- symbolic/action/panel icons taken from:
    - [Tela icon theme](https://github.com/vinceliuice/Tela-icon-theme) by Vince Liuice
- additional application icons taken from:
    - [We10X icon theme](https://github.com/yeyushengfan258/We10X-icon-theme) by yeyushengfan258
    - [Fluent icon theme](https://github.com/vinceliuice/Fluent-icon-theme) by Vince Liuice
    - [Papirus icon theme](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme) by the Papirus Team

See [AUTHORS](AUTHORS) for details.
This combined icon theme is provided in accordance to the GPLv3 license, as are its sources.
All modifications are provided in `build.sh`.

## Build and installation

    ./build.sh
    [sudo] ./install.sh

Using `sudo` (being root) for installation is optional but recommended for the best software compatibility.
The icon theme will be installed as `Win1X` in either `~/.icons` (as normal user) or `/usr/share/icons/` (if you are root).