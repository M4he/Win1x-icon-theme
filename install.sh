#!/bin/bash

ROOT_UID=0
DEST_DIR=

# Destination directory
if [ "$UID" -eq "$ROOT_UID" ]; then
  DEST_DIR="/usr/share/icons"
else
  DEST_DIR="$HOME/.icons"
  mkdir -p "$DEST_DIR"
fi

# -P = never follow symlinks, directly copy them
# -T = treat destination as regular file, do not copy folder into folder
# we use --remove-destination for future updates that may replace former
# symlinks with actual files
cp -rTP --remove-destination build/Win1X "$DEST_DIR/Win1X"
cp -rTP --remove-destination build/Win1X-dark "$DEST_DIR/Win1X-dark"
