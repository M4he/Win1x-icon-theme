#!/usr/bin/env python3
import os
import sys
import shutil

PADDING = "  "

# ./pull_any_symlinks.py <SOURCE_DIR> <ICON> <TARGET_DIR>
#
# Iterates SOURCE_DIR and catches all symlinks that point to ICON
# and copies those symlinks to TARGET_DIR. For any symlink found, this
# procedure is repeated recursively to also include the whole symlink tree
# starting at ICON.

source = sys.argv[1]
icon = sys.argv[2]
target = sys.argv[3]

def pull_symlinks(source, icon, target, level=1):
    for filename in os.listdir(source):
        abspath = os.path.join(source, filename)
        if os.path.islink(abspath) and os.readlink(abspath) == icon:
            print("%s'- %s" % (PADDING * int(level), filename))
            dest = os.path.join(target, filename)
            if os.path.exists(dest) or os.path.islink(dest):
                # prepare overwrite
                os.remove(dest)
            shutil.copy(abspath, dest, follow_symlinks=False)
            pull_symlinks(source, filename, target, level+1)

print("%s'- pulling symlinks for %s ..." % (PADDING, icon))
pull_symlinks(source, icon, target)